%number of simulation
n=10000000

%Firstly, we will construct individual probability distributions 

%probability distribution (pd) of Volume
V_pd = makedist('Triangular','A',10000,'B',40000,'C',100000);

%probability distribution (pd) of Price
p_pd = makedist('Triangular','A',100,'B',110,'C',150);

%probability distribution (pd) of Fixed cost
fc_pd = makedist('Triangular','A',140000,'B',300000,'C',420000);

%probability distribution (pd) of variable cost
vc_pd = makedist('Triangular','A',10,'B',30,'C',105);

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,100)
xlim([0,15000000]);

n=1

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,100)
xlim([0,15000000]);

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,100)
xlim([0,15000000]);

n=1

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,100)
xlim([0,15000000]);

n=100

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,100)
xlim([0,15000000]);

n=100

%to obtain n random values of volume, we will use
V=random(V_pd,1,n);

%similar approach will be applied on remaining variables
p=random(p_pd,1,n);
fc=random(fc_pd,1,n);
vc=random(vc_pd,1,n);

%we will apply the whole formula to obtain n possible results
P=p.*(V-vc)-fc;

%and we will plot the results
hist(P,10)
xlim([0,15000000]);
